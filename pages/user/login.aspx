﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="/masters/Login.Master" %>
<%@ MasterType VirtualPath="~/masters/Login.master" %>
<%@ import namespace="system.data" %>
<%@ import namespace="system.security.cryptography" %>
<%@ import namespace="system.text" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    Dim params As New NameValueCollection
    Dim returnPage As String

    If Session("ReturnPage") Is Nothing Then
        returnPage = "/default.aspx"
    Else
        returnPage = Session("ReturnPage")
    End If

    If Not Request("Email") Is Nothing AndAlso Not Request("Password") Is Nothing Then
        params.Add("@Email", Request("Email"))
        Dim saltResult As DataTable = Master.executeStoreProcedure("Get_Salt", params).Tables(0)

        If Not saltResult Is Nothing AndAlso saltResult.Rows.Count > 0 Then
            Dim saltBytes As Byte() = Convert.FromBase64String(saltResult.Rows(0).Item("Salt"))

            Dim passwordBytes As Byte() = Encoding.UTF8.GetBytes(Request("Password"))

            Dim passwordWithSaltBytes() As Byte = New Byte(passwordBytes.Length + saltBytes.Length - 1) {}

            Dim i As Integer
            For i = 0 To passwordBytes.Length - 1
                passwordWithSaltBytes(i) = passwordBytes(i)
            Next i

            For i = 0 To saltBytes.Length - 1
                passwordWithSaltBytes(passwordBytes.Length + i) = saltBytes(i)
            Next i

            Dim passwordHash As HashAlgorithm = New SHA256Managed()
            Dim passwordHashBytes As Byte() = passwordHash.ComputeHash(passwordWithSaltBytes)
            Dim passwordHashWithSaltBytes() As Byte = New Byte(passwordHashBytes.Length + saltBytes.Length - 1) {}

            For i = 0 To passwordHashBytes.Length - 1
                passwordHashWithSaltBytes(i) = passwordHashBytes(i)
            Next i

            For i = 0 To saltBytes.Length - 1
                passwordHashWithSaltBytes(passwordHashBytes.Length + i) = saltBytes(i)
            Next i

            Dim passwordHashValue As String = Convert.ToBase64String(passwordHashWithSaltBytes)

            params.Add("@Password", passwordHashValue)

            Dim result As DataTable = Master.executeStoreProcedure("Validate_Login", params).Tables(0)

            If Not result Is Nothing AndAlso result.Rows.Count > 0 Then
                Session("LoggedIn") = True
                Session("Email") = result.Rows(0).Item("Email")
                Session("Name") = result.Rows(0).Item("FirstName")
                Response.Redirect(returnPage)
            Else
                Response.Write("<input type='hidden' id='serverError' value='The specified password was not valid.' />")
            End If
        Else
            Response.Write("<input type='hidden' id='serverError' value='Your userame was not found.' />")
        End If


    End If
%>    
    <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui teal image header">
                <img src="/img/0250_OCLW_logo.png" class="image">
                <div class="content">
                    Log-in to your account
                </div>
            </h2>
            <form method="post" action="/pages/user/login.aspx" class="ui large form">
                <input type="hidden" name="returnpage" value="<%=returnPage %>" />
                <div class="ui stacked segment">
                    <div class="ui negative message" id="errorMessage" style="display:none">
                      <div class="header" id="messageText"></div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" id="Email" name="Email" placeholder="Email" value="<%=Request("Email") %>">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                           <input type="password" id="Password" name="Password" placeholder="Password">
                        </div>
                    </div>
                    <input type="submit" value="Login" class="fluid ui green basic button" name="" onclick="return validateForm();">
                </div>

                <div class="ui error message"></div>
            </form>
            
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            var serverError = $("#serverError").val();
            if (serverError != "" && serverError != null)
                displayError(serverError);
        });

        function validateForm() {
            var blnReturn = true;
            var email = $('#Email').val();
            var password = $('#Password').val();
            
            if (email.length < 6 || email.indexOf('@') == -1 || email.indexOf('.') == -1)
            {
                blnReturn = false;
                displayError("Email address is not valid");
            }

            return blnReturn;
        }

        function displayError(msg) {
            $("#errorMessage").html(msg);
            $("#errorMessage").show();
        }
    </script>
</asp:Content>
