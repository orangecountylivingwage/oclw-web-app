﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <form method="post" action="/aspx/user_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="User_Insert" />
        <input type="hidden" name="action" value="insert" /> 
        <div class="five wide field"><h4 class="ui dividing header">User Information</h4></div>
        <div class="fields">
            <div class="five wide field ui negative message" id="errorMessage" style="display:none">
                <div class="header" id="messageText"></div>
            </div>
        </div>
        <div class="fields">
            <div class="five wide field">
                <label>Email</label>
                <input type="text" id="Email" name="Email" placeholder="Email">
            </div>
        </div>   
        <div class="fields">
            <div class="five wide field">
                <label>First Name</label>
                <input type="text" id="FirstName" name="FirstName" placeholder="First Name">
            </div>
        </div>  
        <div class="fields">
            <div class="five wide field">
                <label>Last Name</label>
                <input type="text" id="LastName" name="LastName" placeholder="Last Name">
            </div>
        </div>  
        <div class="fields">
            <div class="five wide field">
                <label>Password</label>
                <input type="password" id="Password" name="Password" placeholder="Password">
            </div>
        </div>  
        <div class="fields">
            <div class="five wide field">
                <label>Confirm Password</label>
                <input type="password" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
            </div>
        </div>  
       <div class="fields">
            <div class="five wide field" style="">
	            <input type="submit" value="Create User" class="fluid ui green basic button" name="" onclick="return validateForm();">
            </div>
        </div>
                
    </form>

    <script type="text/javascript">
        function validateForm() {
            var blnReturn = true;
            var email = $('#Email').val();
            var fname = $('#FirstName').val();
            var lname = $('#LastName').val();
            var password = $('#Password').val();
            var confirm = $('#ConfirmPassword').val();
            var errMessage = "";

            if (email.length < 6 || email.indexOf('@') == -1 || email.indexOf('.') == -1) {
                blnReturn = false;
                errMessage += "Email address is not valid.";
            }

            if (fname == "") {
                blnReturn = false;
                if (errMessage != "")
                    errMessage += "</br>";

                errMessage += "First name is required.";
            }

            if (lname == "") {
                blnReturn = false;
                if (errMessage != "")
                    errMessage += "</br>";

                errMessage += "Last name is required.";
            }

            if (password.length < 8) {
                blnReturn = false;
                if (errMessage != "")
                    errMessage += "</br>";

                errMessage += "Passwords must be at least 8 characters long.";
            }

            if (password != confirm) {
                blnReturn = false;
                if (errMessage != "")
                    errMessage += "</br>";

                errMessage += "Passwords do not match.";
            }

            if (errMessage != "")
                displayError(errMessage);

            return blnReturn;
        }
        function validateForm2() {
            
            
            if (email.length < 6 || email.indexOf('@') == -1 || email.indexOf('.') == -1)
            {
                blnReturn = false;
                alert("Email address is not valid");
            }

            if (fname.length < 1) {
                blnReturn = false;
                alert("First name is required.");
            }

            if (lname.length < 1) {
                blnReturn = false;
                alert("Last name is required.");
            }

            if (password.length < 8)
            {
                blnReturn = false;
                alert("Passwords must be at least 8 characters long.");
            }

            if (password != confirm) {
                blnReturn = false;
                alert("Passwords do not match");
            }

            alert(blnReturn);

            if (errMessage != "")
                displayError(errMessage);

            return blnReturn;
        }

        function displayError(msg) {
            $("#errorMessage").html(msg);
            $("#errorMessage").show();
        }
    </script>
</asp:Content>
