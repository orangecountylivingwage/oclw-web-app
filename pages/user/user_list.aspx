﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%  
    Dim params As New NameValueCollection
    Dim results As DataTable = Master.executeStoreProcedure("User_List", params).Tables(0)



%>
    <table name="mainTable" class="ui celled table">
        <thead>
            <th>Email</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th style="display:none">Menu</th>
        </thead>
        <tbody>
            <%For i As Integer = 0 To results.Rows.Count - 1 %>
                <tr>
                    <td>
                        <%=results.Rows(i).Item("Email") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("FirstName") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("LastName") %>
                    </td>
                    <td style="display:none">
                        <div name="mainTableMenu" class="ui vertical menu">
                          <div class="link item active"><a href="user_edit.aspx?Id=<%=results.Rows(i).Item("Id") %>">Edit</a></div>
                          <div class="link item active"><a href="user_delete.aspx?Id=<%=results.Rows(i).Item("Id") %>">Delete</a></div> 
                          <div class="link item active"><a href="user_chgpw.aspx?Id=<%=results.Rows(i).Item("Id") %>">Change Password</a></div> 
                        </div>
                    </td>
                </tr>

            <%next %>
        </tbody>
    </table>    
    <script>
        $('table').DataTable({
            

        });
        $('[name=mainTable] tbody tr').bind("contextmenu", function (event) {            
            event.preventDefault();

            $('[name=mainTableMenu].active').remove();
          
            var menu = $($(event.currentTarget).find('td').last().html())
            $(menu).addClass('active')
            $('body').append(menu)
            
            $(menu).css({
                position: 'absolute',
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        });
        $(document).click(function (e) {
            if (!$(e.target).parents("[name=mainTableMenu].active").length > 0) {
                $('[name=mainTableMenu].active').remove();
            }
        })
    </script>
</asp:Content>
