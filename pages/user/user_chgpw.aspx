﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <form method="post" action="/aspx/user_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="User_Insert" />
        <input type="hidden" name="action" value="changePassword" />
        <input type="hidden" name="id" value="<%=Request("Id") %>" /> 
        <div class="five wide field"><h4 class="ui dividing header">Change Password</h4></div>
        <div class="five wide field ui negative message" id="errorMessage" style="display:none">
            <div class="header" id="messageText"></div>
        </div>
        <div class="fields">
            <div class="five wide field">
                <label>Password</label>
                <input type="password" id="Password" name="Password" placeholder="Password">
            </div>
        </div>  
        <div class="fields">
            <div class="five wide field">
                <label>Confirm Password</label>
                <input type="password" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
            </div>
        </div>
       <div class="fields">
            <div class="five wide field" style="">
	            <input type="submit" value="Change Password" class="fluid ui green basic button" name="" onclick="return validateForm();">
            </div>
        </div>
                
    </form>

    <script type="text/javascript">
        function validateForm() {
            var blnReturn = true;
            var password = $('#Password').val();
            var confirm = $('#ConfirmPassword').val();
            var errMessage = "";

            if (password.length < 8)
            {
                blnReturn = false;
                errMessage = "Passwords must be at least 8 characters long.";
            }

            if (password != confirm) {
                blnReturn = false;
                if (errMessage != "")
                    errMessage += "</br>"
                
                errMessage += "Passwords do not match.";
            }

            if (errMessage != "")
                displayError(errMessage);

            return blnReturn;
        }

        function displayError(msg) {
            $("#errorMessage").html(msg);
            $("#errorMessage").show();
        }
    </script>
</asp:Content>
