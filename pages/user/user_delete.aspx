﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    Dim params As New NameValueCollection


    If Request("Id") Is Nothing Then
        Response.Redirect("/pages/user/user_list.aspx")
    End If

    params.Add("@Id", Request("Id"))
    Dim results As DataTable = Master.executeStoreProcedure("User_By_Id", params).Tables(0)

    If results.Rows(0).Item("Email") = Session("Email") Then
        Response.Write("<script language=""javascript"">alert('You cannot delete your own account.');</script>")
        Response.Redirect("/pages/user/user_list.aspx")
    End If
%>
    <form method="post" action="/aspx/user_update.aspx" class="ui form">
        <input type="hidden" name="action" value="delete" /> 
        <input type="hidden" name="id" value="<%=Request("Id") %>" />
        <div class="five wide field"><h4 class="ui dividing header">Delete User</h4></div>
        <div class="fields">
            <div class="five wide field">
                <label>Email</label>
                <label><%=results.Rows(0).Item("Email") %></label>
            </div>
        </div>   
        <div class="fields">
            <div class="five wide field">
                <label>First Name</label>
                <label><%=results.Rows(0).Item("FirstName") %></label>
            </div>
        </div>  
        <div class="fields">
            <div class="five wide field">
                <label>Last Name</label>
                <label><%=results.Rows(0).Item("LastName") %></label>
            </div>
        </div>  
       <div class="fields">
            <div class="five wide field" style="">
	            <input type="submit" value="Delete User" class="fluid ui green basic button" name="" onclick="return validateForm();">
            </div>
        </div>
                
    </form>

    <script type="text/javascript">
        function validateForm() {
            var blnReturn = false;
            
            if (confirm("Are you sure you want to delete this user? This action cannot be undone."))
                blnReturn = true;

            return blnReturn;
        }
    </script>
</asp:Content>
