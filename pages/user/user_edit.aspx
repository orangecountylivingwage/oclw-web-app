﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    Dim params As New NameValueCollection



    If Request("Id") Is Nothing Then
        Response.Redirect("/pages/user/user_list.aspx")
    End If

    params.Add("@Id", Request("Id"))
    Dim results As DataTable = Master.executeStoreProcedure("User_By_Id", params).Tables(0)
%>
    <form method="post" action="/aspx/user_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="User_Update" />
        <input type="hidden" name="action" value="update" /> 
        <input type="hidden" name="id" value="<%=Request("Id") %>" /> 
        <div class="five wide field"><h4 class="ui dividing header">User Information</h4></div>
        <div class="fields">
            <div class="five wide field ui negative message" id="errorMessage" style="display:none">
                <div class="header" id="messageText"></div>
            </div>
        </div>
        <div class="fields">
            <div class="five wide field">
                <label>Email</label>
                <input type="text" id="Email" name="Email" placeholder="Email" value="<%=results.Rows(0).Item("Email") %>">
            </div>
        </div>
        <div class="fields">
            <div class="five wide field">
                <label>First Name</label>
                <input type="text" id="FirstName" name="FirstName" placeholder="First Name" value="<%=results.Rows(0).Item("FirstName") %>">
            </div>
        </div>  
        <div class="fields">
            <div class="five wide field">
                <label>Last Name</label>
                <input type="text" id="LastName" name="LastName" placeholder="Last Name" value="<%=results.Rows(0).Item("LastName") %>">
            </div>
        </div>  
       <div class="fields">
            <div class="five wide field" style="">
	            <input type="submit" value="Update User" class="fluid ui green basic button" name="" onclick="return validateForm();">
            </div>
        </div>
                
    </form>

    <script type="text/javascript">
        function validateForm() {
            blnReturn = true;
            var email = $('#Email').val();
            var fname = $('#FirstName').val();
            var lname = $('#LastName').val();
            var errMessage = "";

            if (email.length < 6 || email.indexOf('@') == -1 || email.indexOf('.') == -1) {
                blnReturn = false;
                errMessage += "Email address is not valid.";
            }

            if (fname == "") {
                blnReturn = false;
                if (errMessage != "")
                    errMessage += "</br>";

                errMessage += "First name is required.";
            }

            if (lname == "") {
                blnReturn = false;
                if (errMessage != "")
                    errMessage += "</br>";

                errMessage += "Last name is required.";
            }

            if (errMessage != "")
                displayError(errMessage);

            return blnReturn;
        }

        function displayError(msg) {
            $("#errorMessage").html(msg);
            $("#errorMessage").show();
        }
    </script>
</asp:Content>
