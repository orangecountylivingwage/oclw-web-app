﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    Session.Remove("LoggedIn")
    Session.Remove("Email")
    Session.Remove("Name")
    Response.Redirect("/pages/user/login.aspx")
%>
    
</asp:Content>
