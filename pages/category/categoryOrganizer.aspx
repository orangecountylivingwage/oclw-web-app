﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.data" %>
<script runat="server">
    Function loadAccordian(ByVal id As Integer) As String
        Dim resultString = ""
        Dim params As New NameValueCollection
        params.Add("@ParentId", id)
        Dim subCategories As DataTable = Master.executeStoreProcedure("Category_By_ParentId", params).Tables(0)
        If subCategories.Rows().Count > 0 Then
            For i As Integer = 0 To subCategories.Rows().Count - 1
                resultString = resultString & "<div class=""title""><i class=""dropdown icon""></i>" & subCategories.Rows(i).Item("CategoryName") & "</div>"
                resultString = resultString & "<div class=""content"">" & loadAccordian(subCategories.Rows(i).Item("id")) & "</div>"
            Next
        Else
            params.Clear()
            params.Add("@CategoryId", id)
            Dim jobs As DataTable = Master.executeStoreProcedure("Employer_By_CategoryId", params).Tables(0)
            For i As Integer = 0 To jobs.Rows().Count - 1
                resultString = resultString & "<div class=""job"">" & jobs.Rows(i).Item("EmployerName") & "</div>"
            Next
        End If

        Return resultString
    End Function
</script>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .job {
            margin: 0 0 .3em 0;
            padding: .5em 1em .5em;
            box-shadow: 0 1px 2px 0 rgba(34,36,38,.15),0 0 0 1px rgba(34,36,38,.15);
        }
    </style>
    <div class="ui styled accordion">
        <%=loadAccordian(Request("id")) %>
    </div>
    <script>
        $('.accordion').accordion({
            exclusive: false
        })
    </script>

</asp:Content>
