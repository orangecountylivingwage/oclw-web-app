﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server"> 
  <%
      Dim params As New NameValueCollection
      params.Add("@Id", Request("Id"))
      Dim results As DataTable = Master.executeStoreProcedure("Category_By_Id", params).Tables(0) %>
    <form method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="Category_Edit" />
        <input type="hidden" name="Id" value="<%=results.Rows(0).Item("Id") %>" /> 
        <input type="hidden" name="edit" value="2" /> 
        <input type="hidden" name="return" value="Id" />          
        <input type="hidden" name="return2" value="CategoryName" />               
        <h4 class="ui dividing header">Category Information</h4>     
        <div class="fields">
            <div class="sixteen wide field">
                <label>Name</label>
                <input type="text" name="CategoryName" value="<%=results.Rows(0).Item("CategoryName") %>" placeholder="Name">
            </div>      
                         
        </div>         
        </form>    
        
           
   
        
                  
    
    <script>
        $('select.dropdown')
  .dropdown()
        ;
   
    </script>
</asp:Content>
