﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <%
       Dim params As New NameValueCollection
       params.Add("@Id", Request("Id"))
       Dim results As DataTable = Master.executeStoreProcedure("Category_By_Id", params).Tables(0) %>
    <form method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="Category_Edit" />
        <input type="hidden" name="Id" value="<%=results.Rows(0).Item("Id") %>" /> 
        <input type="hidden" name="edit" value="3" />
        <input type="hidden" name="return" value="Id" />
        <div class="description">
          <p>Are you sure you want to delete this Category? Before a Category can be deleted, all businesses and categories must be removed from it, and all conflicts must be resolved.</p>
        </div>    
        </form>   
</asp:Content>
