﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server"> 
  <%
      Dim params As New NameValueCollection
      Dim results As DataTable = Master.executeStoreProcedure("Category_Log", params).Tables(0) %>
    <form method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="Category_Edit" />
        <input type="hidden" name="edit" value="1" /> 
        <input type="hidden" name="return" value="Id" />   
        <input type="hidden" name="return2" value="ParentId" />
        <input type="hidden" name="return3" value="CategoryName" />               
        <h4 class="ui dividing header">Category Information</h4>     
        <div class="fields">
            <div class="ten wide field">
                <label>Name</label>
                <input type="text" name="CategoryName" placeholder="Name">
            </div>       
            <div class="six wide field">
                <label>Category</label>
                <select name="ParentId" class="ui search dropdown">
                    <%For i As Integer = 0 To results.Rows().Count - 1%>                           
                     <option value="<%=results.Rows(i).Item("id") %>"><%=results.Rows(i).Item("CategoryName") %></option>
                    <%next %>
                </select>
            </div>                 
        </div>         
        </form>    
        
           
   
        
                  
    
    <script>
        $('select.dropdown')
  .dropdown()
        ;
   
    </script>
</asp:Content>
