﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Categories" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<script runat="server">
    Function loadAccordian(ByVal id As Integer) As String
        Dim resultString = ""
        Dim params As New NameValueCollection
        params.Add("@ParentId", id)
        Dim subCategories As DataTable = Master.executeStoreProcedure("Category_By_ParentId", params).Tables(0)
        If subCategories.Rows().Count > 0 Then
            resultString = resultString & "<div categoryId=""" & id & """ class=""upper accordion"">"
        Else
            resultString = resultString & "<div categoryId=""" & id & """ class=""accordion"">"
        End If

        If subCategories.Rows().Count > 0 Then
            For i As Integer = 0 To subCategories.Rows().Count - 1
                If id <> -1 Then
                    resultString = resultString & "<div categoryId=""" & subCategories.Rows(i).Item("id") & """ class=""draggable title""><i class=""dropdown icon""></i><span>" & subCategories.Rows(i).Item("CategoryName") & "</span></div>"
                Else
                    resultString = resultString & "<div categoryId=""" & subCategories.Rows(i).Item("id") & """ class=""title""><i class=""dropdown icon""></i><span>" & subCategories.Rows(i).Item("CategoryName") & "</span></div>"
                End If
                resultString = resultString & "<div categoryId=""" & subCategories.Rows(i).Item("id") & """ class=""content droppable"">" & loadAccordian(subCategories.Rows(i).Item("id")) & "</div>"

            Next
        Else
            params.Clear()
            params.Add("@CategoryId", id)
            Dim jobs As DataTable = Master.executeStoreProcedure("Employer_By_CategoryId", params).Tables(0)
            For i As Integer = 0 To jobs.Rows().Count - 1
                resultString = resultString & "<div emp_id=""" & jobs.Rows(i).Item("id") & """ class=""draggable emp"">" & jobs.Rows(i).Item("EmployerName") & "</div>"
            Next
        End If
        resultString = resultString & "</div>"
        Return resultString
    End Function
</script>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
            <%  
                Dim params As New NameValueCollection
                Dim results As DataTable = Master.executeStoreProcedure("User_List", params).Tables(0)

                If Session("LoggedIn") Is Nothing OrElse Session("LoggedIn") = False Then
                    Session("ReturnPage") = "pages/category/category.aspx?"
                    Response.Redirect("/pages/user/login.aspx")
                End If

%>
    <style>
        button.hidden {
            display: none !important;
        }
        .upper > .emp {
            background-color:rgba(255, 0, 0, 0.38);
        }
        .content.highlight {
            box-shadow: 1px 1px 11px 1px rgb(255, 141, 0) inset;
        }

        .emp {
            margin: 0 0 .3em 0;
            padding: .5em 1em .5em;
            box-shadow: 0 1px 2px 0 rgba(34,36,38,.15),0 0 0 1px rgba(34,36,38,.15);
        }
    </style>
    <div class="buttonContainer">
        <button name="enableButton" onclick="enableEditing()" class="ui green button">
            Enable Editing
        </button>
        <button name="saveButton" onclick="save()" class="hidden ui green button">
            Save
        </button>
        <button name="addCategoryButton" onclick="newCategory()" class="ui yellow button" style="float:right;">
            Add Category
        </button>
        <button name="cancelButton" onclick="cancelEditing()" class="hidden ui red button">
            Cancel
        </button>
    </div>
    
        <div class="ui fluid styled accordion">
        <%=loadAccordian(-1) %>
    </div>
    <script>
        $('.fluid.accordion').accordion({
            exclusive: false
        })
        $(document).click(function (e) {
            if (!$(e.target).parents("[name=mainTableMenu].active").length > 0) {
                $('[name=mainTableMenu].active').remove();
            }
        })
        $('.content .title').bind("contextmenu", "tr", function (event) {
            event.preventDefault();

            $('[name=mainTableMenu].active').remove();
            var menu = $('' +
            '<div name="mainTableMenu" class="ui vertical menu">' +
                    '<div class="link item active editElement">' +
                    'Edit Category' +
                '</div>' +
                '<div class="link item active deleteElement">' +
                    'Delete Category' +
                '</div>' +
            '</div>');
            var element = $(event.currentTarget)           
            $(menu).addClass('active')
            var buttonOne = $(menu).find('.link')[0]
            var buttonTwo = $(menu).find('.link')[1]
            $(buttonOne).click(function () { editCategory(element) })
            $(buttonTwo).click(function () { deleteCategory(element) })
            $('body').append(menu)
            $(menu).css({
                position: 'absolute',
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        });
        $('[emp_id]').bind("contextmenu", "tr", function (event) {
            event.preventDefault();

            $('[name=mainTableMenu].active').remove();
            var menu = $('' +
            '<div name="mainTableMenu" class="ui vertical menu">' +
                    '<div class="link item active cloneElement">' +
                    'Clone Element' +
                '</div>' +
            '</div>');
            var element = $(event.currentTarget)           
            $(menu).addClass('active')
            var buttonOne = $(menu).find('.link')
            $(buttonOne).click(function (){cloneElement(element)})
            $('body').append(menu)
            $(menu).css({
                position: 'absolute',
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        });
        $('.draggable').draggable({
            containment: 'document',
            revert: "invalid",
            helper: "clone",
            disabled: true
        })
        $('.droppable').droppable({
            tolerance: "pointer",
            accept: function (elem) {
                // check elem here for being a child and return false   
                if ($(elem).hasClass('title')) {
                    var categoryContent = $('.content[categoryid=' + $(elem).attr('categoryid') + ']')[0]
                    return !($(this).is(categoryContent) || $.contains(categoryContent, this));
                } else {
                    return true;
                }
            },
            hoverClass: "highlight",
            greedy: true,
            drop: function (event, ui) {
                var elem = $(ui.draggable)
                var previousAccordion = elem.parent()[0]               
                
                var categoryId = $(elem).attr('categoryid')
                var content = $('.content[categoryid=' + categoryId + ']')
                var drop = $(this)
                var droppedAccordion = $(drop).children('.accordion')[0]
                $(droppedAccordion).append(elem);
                $(droppedAccordion).append(content);
                if ($(elem).hasClass('title'))
                    $(droppedAccordion).addClass('upper')               
                if ($(previousAccordion).children('.title').length == 0) {
                    $(previousAccordion).removeClass('upper')
                }
            }
        })
        function enableEditing() {
            $('[name=saveButton]').removeClass('hidden')
            $('[name=cancelButton]').removeClass('hidden')
            $('[name=enableButton]').addClass('hidden')
            $('.draggable').draggable("enable")

        }
        function cloneElement(element) {
            var cloned = $(element).clone()
            $(cloned).draggable({
                containment: 'document',
                revert: "invalid",
                helper: "clone",
                disabled: false
            })
            $(cloned).bind("contextmenu", "tr", function (event) {
                event.preventDefault();

                $('[name=mainTableMenu].active').remove();
                var menu = $('' +
                '<div name="mainTableMenu" class="ui vertical menu">' +
                        '<div class="link item active cloneElement">' +
                        'Clone Element' +
                    '</div>' +
                '</div>');
                var element = $(event.currentTarget)
                $(menu).addClass('active')
                var buttonOne = $(menu).find('.link')
                $(buttonOne).click(function () { cloneElement(element) })
                $('body').append(menu)
                $(menu).css({
                    position: 'absolute',
                    top: event.pageY + "px",
                    left: event.pageX + "px"
                });
            });
            var accordion = $(element).parent()
            $(accordion).append(cloned)
        }
        function editCategory(element) {
            var modal = $('<div class="ui modal" >' +
                               ' <div class="header">' +
                                 ' Edit Category' +
                               ' </div>')
            var content = $(' <div class="content"></div>')

            var buttons = $(' <div class="actions">' +
             ' <div class="ui negative button">' +
             '   Cancel' +
             ' </div>' +
              '<div class="ui positive approve right labeled icon button">' +
               ' Save' +
               ' <i class="checkmark icon"></i>' +
             ' </div>' +
           ' </div>' +
         ' </div>')
            $(modal).append(content)
            $(modal).append(buttons)
            $(content).load('/pages/category/popup/category_edit.aspx?Id=' + $(element).attr('categoryid'),
                 function () {

                     $(modal).modal({
                         onApprove: function () {
                             var formdata = $(this).find('form').serialize();
                             $.ajax({
                                 url: "/aspx/application_update.aspx",
                                 data: formdata
                             }
                                 ).done(function (data) {
                                     data = $.parseJSON(data);
                                     var header = $('.title[categoryid=' + data.Id + ']').find('span')
                                     $(header).text(data.CategoryName)
                                    
                                   
                                 })
                         }
                     }).modal('show')
                 })
        }
        function deleteCategory(element) {
            var modal = $('<div class="ui modal" >' +
                               ' <div class="header">' +
                                 ' Delete Category' +
                               ' </div>')
            var content = $(' <div class="content"></div>')

            var buttons = $(' <div class="actions">' +
             ' <div class="ui negative button">' +
             '   Cancel' +
             ' </div>' +
              '<div class="ui positive approve right labeled icon button">' +
               ' Yes' +
               ' <i class="checkmark icon"></i>' +
             ' </div>' +
           ' </div>' +
         ' </div>')
            $(modal).append(content)
            $(modal).append(buttons)
            $(content).load('/pages/category/popup/category_delete.aspx?Id=' + $(element).attr('categoryid'),
                 function () {

                     $(modal).modal({
                         onApprove: function () {
                             var formdata = $(this).find('form').serialize();
                             var id = $(this).find('form').find('input[name=Id]').val();
                             if ($('.upper > .emp').length == 0 && $('.content[categoryid=' + id + ']').find('.title').length == 0 && $('.content[categoryid=' + id + ']').find('.emp').length == 0) {
                             $.ajax({
                                 url: "/aspx/application_update.aspx",
                                 data: formdata
                             }
                                 ).done(function (data) {
                                     data = $.parseJSON(data);
                                     var header = $('.title[categoryid=' + data.Id + ']')
                                     var content = $('.content[categoryid=' + data.Id + ']')
                                     $(header).remove()
                                     $(content).remove()
                                     $('[name=saveButton]').removeClass('hidden')                                     
                                     save()


                             })
                         }
                     }
                     }).modal('show')
        })
        }

        function newCategory() {
            var modal = $('<div class="ui modal" >' +
                               ' <div class="header">' +
                                 ' New Category' +
                               ' </div>')
            var content = $(' <div class="content"></div>')

            var buttons = $(' <div class="actions">' +
             ' <div class="ui negative button">' +
             '   Cancel' +
             ' </div>' +
              '<div class="ui positive approve right labeled icon button">' +
               ' Save' +
               ' <i class="checkmark icon"></i>' +
             ' </div>' +
           ' </div>' +
         ' </div>')
            $(modal).append(content)
            $(modal).append(buttons)
            $(content).load('/pages/category/popup/category_new.aspx', function () {

                $(modal).modal({
                    onApprove: function () {
                        var formdata = $(this).find('form').serialize();
                        $.ajax({
                            url: "/aspx/application_update.aspx",
                            data: formdata
                        }
                            ).done(function (data) {
                                data = $.parseJSON(data);
                                var header = $('<div categoryId="' + data.Id + '" class="draggable title"><i class="dropdown icon"></i><span>' + data.CategoryName + '</span></div>')
                                var accordion = $('<div categoryId="' + data.Id + '" class="content droppable"><div categoryId="' + data.Id + '" class="accordion"></div></div>')
                                
                                $(header).draggable({
                                    containment: 'document',
                                    revert: "invalid",
                                    helper: "clone",
                                    disabled: false
                                })
                                $(header).bind("contextmenu", "tr", function (event) {
                                    event.preventDefault();

                                    $('[name=mainTableMenu].active').remove();
                                    var menu = $('' +
                                    '<div name="mainTableMenu" class="ui vertical menu">' +
                                            '<div class="link item active editElement">' +
                                            'Edit Category' +
                                        '</div>' +
                                        '<div class="link item active deleteElement">' +
                                            'Delete Category' +
                                        '</div>' +
                                    '</div>');
                                    var element = $(event.currentTarget)
                                    $(menu).addClass('active')
                                    var buttonOne = $(menu).find('.link')[0]
                                    var buttonTwo = $(menu).find('.link')[1]
                                    $(buttonOne).click(function () { editCategory(element) })
                                    $(buttonTwo).click(function () { deleteCategory(element) })
                                    $('body').append(menu)
                                    $(menu).css({
                                        position: 'absolute',
                                        top: event.pageY + "px",
                                        left: event.pageX + "px"
                                    });
                                });
                                $(accordion).droppable({
                                    tolerance: "pointer",
                                    accept: function (elem) {
                                        // check elem here for being a child and return false   
                                        if ($(elem).hasClass('title')) {
                                            var categoryContent = $('.content[categoryid=' + $(elem).attr('categoryid') + ']')[0]
                                            return !($(this).is(categoryContent) || $.contains(categoryContent, this));
                                        } else {
                                            return true;
                                        }
                                    },
                                    hoverClass: "highlight",
                                    greedy: true,
                                    drop: function (event, ui) {
                                        var elem = $(ui.draggable)
                                        var previousAccordion = elem.parent()[0]

                                        var categoryId = $(elem).attr('categoryid')
                                        var content = $('.content[categoryid=' + categoryId + ']')
                                        var drop = $(this)
                                        var droppedAccordion = $(drop).children('.accordion')[0]
                                        $(droppedAccordion).append(elem);
                                        $(droppedAccordion).append(content);
                                        if ($(elem).hasClass('title'))
                                            $(droppedAccordion).addClass('upper')
                                        if ($(previousAccordion).children('.title').length == 0) {
                                            $(previousAccordion).removeClass('upper')
                                        }
                                    }
                                })
                                var parentAccordion = $('.accordion[categoryid=' + data.ParentId + ']')[0]
                                $(parentAccordion).addClass('upper')
                                $(parentAccordion).append(header)
                                $(parentAccordion).append(accordion)
                            })
                    }
                }).modal('show')
            });

        }
        var savingNumber = 0
        function save() {
            if ($('.upper > .emp').length == 0) {
                $('[name=saveButton]').addClass('loading')
                $('.draggable').draggable('disable')

                $('.accordion[categoryid]').each(function (index, item) {
                    var employersarr = [];
                    $(item).children('.emp').each(function (index2, item2) {
                        employersarr.push($(item2).attr('emp_id'))
                    })
                    var categorysarr = [];
                    $(item).children('.title').each(function (index2, item2) {
                        categorysarr.push($(item2).attr('categoryid'))
                    })
                    savingNumber += 1;
                    $.ajax({
                        url: '/aspx/application_update.aspx',
                        data: {id:$(item).attr('categoryid'), procname: 'Category_Edit', edit: 4, employers: employersarr.join(','), categories: categorysarr.join(',')}
                    }).done(function(){
                        savingNumber -= 1;                        
                        if (savingNumber == 0){
                            $('[name=saveButton]').addClass('hidden')
                            $('[name=saveButton]').removeClass('loading')
                            $('[name=cancelButton]').addClass('hidden')
                            $('[name=enableButton]').removeClass('hidden')
                            $.ajax({
                                url: '/aspx/application_update.aspx',
                                data: { procname: 'CalculateBottomCategories'}
                            })
                        }
                    })
                })
            }
        }
        function cancelEditing() {
            $('[name=saveButton]').addClass('hidden')
            $('[name=cancelButton]').addClass('hidden')
            $('[name=enableButton]').removeClass('hidden')
            $('.draggable').draggable('disable')
        }
    </script>
</asp:Content>
