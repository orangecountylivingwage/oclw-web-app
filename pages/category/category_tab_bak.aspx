﻿<%-- Developed By: Dan Piccot --%>
-<%@ Page Title="Categories" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%Dim params As New NameValueCollection
        params.Add("@ParentId", -1)
        Dim topLevel As DataTable = Master.executeStoreProcedure("Category_By_ParentId", params).Tables(0) %>
    <div class="ui top attached tabular menu">
        <div onclick="loadCategory(this,<%=topLevel.Rows(0).Item("id") %>)" class="active item"><%=topLevel.Rows(0).Item("CategoryName") %></div>
        <%for i As Integer = 1 To 6%>
            <div onclick="loadCategory(this,<%=topLevel.Rows(i).Item("id") %>)" class="item"><%=topLevel.Rows(i).Item("CategoryName") %></div>
        <%  Next %>
    </div>
    <div name="tabContent" class="ui bottom attached active tab segment">
        
    </div>
    <script>
        function loadCategory(element,id){
            $('.active.item').removeClass("active")
            $(element).addClass("active");
            $('[name=tabContent]').addClass("loading").load('/pages/category/categoryOrganizer.aspx?id=' + id, function(){
                $('[name=tabContent]').removeClass("loading")
            })
        }
        loadCategory($('.active.item'),<%=topLevel.Rows(0).Item("id") %>)
    </script>
</asp:Content>
