﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%
        Dim params As New NameValueCollection
        params.Add("@Id", Request("id"))
        Dim results2 As DataTable = Master.executeStoreProcedure("Employer_By_Id", params).Tables(0)

        %>

     
  
    <form method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="Employer_Edit" />
        <input type="hidden" name="edit" value="3" /> 
        <input type="hidden" name="Id" value="<%=results2.Rows(0).Item("Id") %>" /> 
        <input type="hidden" name="target" value="business/business_log" /> 
        <div class="description">
          <p>Are you sure you want to delete this Business?</p>
        </div>    
        </form>   
</asp:Content>
