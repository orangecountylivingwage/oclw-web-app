﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <form method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="EmployerLocation_Edit" />
        <input type="hidden" name="edit" value="3" />
        <input type="hidden" name="return" value="Id" />
        <input type="hidden" name="Id" value="<%=Request("id") %>" />
        <input type="hidden" name="target" value="business/business_manage" />
        <div class="description">
            <p>Are you sure you want to delete this Business Location? (Note: You cannot delete the last location on a business)</p>
        </div>
    </form>
</asp:Content>
