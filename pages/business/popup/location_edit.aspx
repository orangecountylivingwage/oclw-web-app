﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%
        Dim params As New NameValueCollection
        Dim States As DataTable = Master.executeStoreProcedure("State_Log", params).Tables(0)
        params.Add("@Id", Request("id"))
        Dim Location As DataTable = Master.executeStoreProcedure("EmployerLocation_By_Id", params).Tables(0)

        %>
    <form name="editLocation" method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="EmployerLocation_Edit" />
        <input type="hidden" name="edit" value="2" />
        <input type="hidden" name="Id" value="<%=Request("Id")%>" />
        <input type="hidden" name="return" value="Id" />
        <input type="hidden" name="target" value="business/business_manage" />
        <h4 class="ui dividing header">Location Information</h4>
         <div class="fields">
            <div class="required eight wide field">
                <label for="LocationName">Location Name</label>
                <input type="text" name="LocationName" id="LocationName" placeholder="Location Name" value="<%=Location.Rows(0).Item("LocationName")%>">
            </div>
            <div class="required eight wide field">
                <label>Phone</label>
                <input type="text" name="Phone" placeholder="Phone" value="<%=Location.Rows(0).Item("Phone")%>">
            </div>
        </div>
        <div class="field">
            <label for="ShortDescription">Override Description</label>
            <textarea maxlength="255" name="ShortDescription" id="ShortDescription"><%=Location.Rows(0).Item("ShortDescription2")%></textarea>
        </div>
        <h4 class="ui dividing header">Location Address</h4>
        <div class="fields">
            <div class="required eight wide field">
                <label for="Address">Address 1</label>
                <input type="text" name="Address" id="Address" placeholder="Address" value="<%=Location.Rows(0).Item("Address")%>">
            </div>
            <div class="eight wide field">
                <label for="Address2">Address 2</label>
                <input type="text" name="Address2" id="Address2" placeholder="Address 2" value="<%=Location.Rows(0).Item("Address2")%>">
            </div>
        </div>
        <div class="fields">
            <div class="required six wide field">
                <label for="City">City</label>
                <input type="text" name="City" id="City" placeholder="City" value="<%=Location.Rows(0).Item("City")%>">
            </div>
            <div class="required six wide field">
                <label for="State">State</label>
                <select name="State" id="State" class="ui search dropdown">
                    <option value="">State</option>
                    <option value="">State</option>
                    <%For i As Integer = 0 To States.Rows().Count - 1%>
                    <%If Not IsDBNull(Location.Rows(0).Item("State")) AndAlso States.Rows(i).Item("StateAbbrev") = Location.Rows(0).Item("State") Then%>
                            <option value="<%=States.Rows(i).Item("StateAbbrev") %>" selected><%=States.Rows(i).Item("StateName") %></option>
                            <%else %>
                            <option value="<%=States.Rows(i).Item("StateAbbrev") %>"><%=States.Rows(i).Item("StateName") %></option>
                            <%End if%>
                    <%next %>
                </select>
            </div>
            <div class="required four wide field">
                <label for="Zip">Zip</label>
                <input type="text" name="Zip" id="Zip" placeholder="Zip" value="<%=Location.Rows(0).Item("Zip")%>">
            </div>
        </div>
         <div class="fields">
            <div class="two wide field">
                <label>GeoLocate</label>
                <input onclick="updateLatLng2(this)" type="button" value="Geolocate" class="fluid ui blue button">
            </div>
            <script>
                function updateLatLng2(button) {
                    $(button).addClass('loading')
                    var form = $('[name=editLocation]')[0]
                    var address = $(form).find('[name=Address]').val() + ' ' +
                        $(form).find('[name=Address2]').val() + ', ' +
                        $(form).find('[name=City]').val() + ', ' +
                        $(form).find('[name=State]').val() + $(form).find('[name=Zip]').val()
                    geoLocate(address, function (data) {
                        var form = $('[name=editLocation]')[0]
                        $(form).find('[name=Lat]').val(data.lat)
                        $(form).find('[name=Long]').val(data.long)
                    }, button);
                }
            </script>
            <div class="required seven wide field">
                <label>Latitude</label>
                <input type="text" name="Lat" placeholder="Latitude" value="<%=Location.Rows(0).Item("Lat")%>">
            </div>
            <div class="required seven wide field">
                <label>Longitude</label>
                <input type="text" name="Long" placeholder="Longitude" value="<%=Location.Rows(0).Item("Long")%>">
            </div>
        </div>
        <div class="sixteen wide column" style="">
            <div class="ui error message"></div>
        </div>
    </form>    

    <script>
        $('select.dropdown')
  .dropdown()
        ;
        $('form[name=editLocation]').find('[name=Phone]').mask('(000) 000-0000')
        $('form[name=editLocation]').form({
            on: 'blur',
            fields: {
                LocationName: {
                    identifier: 'LocationName',
                    rules: [
                      {
                          type: 'empty',
                          prompt: 'Please enter a Location Name.'
                      }
                    ]
                },
                Address: {
                    identifier: 'Address',
                    rules: [
                      {
                          type: 'empty',
                          prompt: 'Please enter an Address.'
                      }
                    ]
                },
                City: {
                    identifier: 'City',
                    rules: [
                      {
                          type: 'empty',
                          prompt: 'Please enter a City.'
                      }
                    ]
                },
                State: {
                    identifier: 'State',
                    rules: [
                      {
                          type: 'empty',
                          prompt: 'Please select a State.'
                      }
                    ]
                },
                Zip: {
                    identifier: 'Zip',
                    rules: [
                      {
                          type: 'empty',
                          prompt: 'Please enter a Zip.'
                      }
                    ]
                }
            },
            Lat: {
                identifier: 'Lat',
                rules: [
                  {
                      type: 'empty',
                      prompt: 'Please enter a Latitude.'
                  }
                ]
            },
            Long: {
                identifier: 'Long',
                rules: [
                  {
                      type: 'empty',
                      prompt: 'Please enter a Longitude.'
                  }
                ]
            }
        });
    </script>
</asp:Content>
