﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" Debug="true" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%
        Dim params As New NameValueCollection
        Dim results As DataTable = Master.executeStoreProcedure("State_Log", params).Tables(0)
        Dim results3 As DataTable = Master.executeStoreProcedure("Category_By_Bottom", params).Tables(0)
        params.Add("@Id", Request("id"))
        Dim results2 As DataTable = Master.executeStoreProcedure("Employer_By_Id", params).Tables(0)

        %>

     
  
    <form name="editBusiness" method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="Employer_Edit" />
        <input type="hidden" name="edit" value="2" />
        <input type="hidden" name="Id" value="<%=results2.Rows(0).Item("Id") %>" />
        <input type="hidden" name="target" value="business/business_log" />
        <h4 class="ui dividing header">Business Information</h4>
        <div class="fields">
            <div class="required ten wide field">
                <label for="EmployerName">Name</label>
                <input id="EmployerName" type="text" name="EmployerName" value="<%=results2.Rows(0).Item("EmployerName") %>" placeholder="Name">
            </div>
            <div class="required six wide field">
                <label>Category</label>
                <select name="Categories" class="ui search dropdown" multiple>
                    <%For i As Integer = 0 To results3.Rows().Count - 1%>
                    <%If Not IsDBNull(results2.Rows(0).Item("CategoryCSV")) AndAlso Array.Exists(results2.Rows(0).Item("CategoryCSV").ToString().Split(","), Function(element)
                                                                                                                                                                   Return element.Equals(results3.Rows(i).Item("id").ToString())
                                                                                                                                                               End Function) Then%>
                    <option value="<%=results3.Rows(i).Item("id") %>" selected><%=results3.Rows(i).Item("CategoryAndParent") %></option>
                    <%else %>
                    <option value="<%=results3.Rows(i).Item("id") %>"><%=results3.Rows(i).Item("CategoryAndParent") %></option>
                    <%end if %>
                    <%next %>
                </select>
            </div>
        </div>
        <div class="fields">
            <div class="required six wide field">
                <label for="Phone">Phone</label>
                <input id="Phone" type="text" value="<%=results2.Rows(0).Item("Phone") %>" name="Phone" placeholder="Phone">
            </div>
            <div class="five wide field">
                <label for="Website">Website</label>
                <input id="Website" type="text" name="Website" value="<%=results2.Rows(0).Item("Website") %>" placeholder="Website">
            </div>
            <div class="five wide field">
                <label for="Email">Email</label>
                <input id="Email" type="text" name="Email" value="<%=results2.Rows(0).Item("Email") %>" placeholder="Email">
            </div>
        </div>
        <div class="required field">
            <label for="ShortDescription">Description</label>
            <textarea id="ShortDescription" maxlength="255" name="ShortDescription"><%=results2.Rows(0).Item("ShortDescription") %></textarea>
        </div>        
        <h4 class="ui dividing header">Business Address</h4>
        <%If results2.Rows(0).Item("NumberOfLocations") > 1 Then %>
        <div class="ui message">
            <div class="header">
                Multiple Locations
            </div>
            Updating this address will not change the address of the Business Location.
        </div>
        <%End if %>
        <div class="fields">
            <div class="required eight wide field">
                <label for="Address">Address 1</label>
                <input id="Address" type="text" name="Address" value="<%=results2.Rows(0).Item("Address") %>" placeholder="Address">
            </div>
            <div class="eight wide field">
                <label for="Address2">Address 2</label>
                <input id="Address2" type="text" name="Address2" value="<%=results2.Rows(0).Item("Address2") %>" placeholder="Address 2">
            </div>
        </div>
        <div class="fields">
            <div class="required six wide field">
                <label for="City">City</label>
                <input id="City" type="text" name="City" value="<%=results2.Rows(0).Item("City") %>" placeholder="City">
            </div>
            <div class="required six wide field">
                <label>State</label>
                <select name="State" class="ui search dropdown">
                    <option value="">State</option>
                    <%For i As Integer = 0 To results.Rows().Count - 1%>
                    <%If Not IsDBNull(results2.Rows(0).Item("State")) AndAlso results.Rows(i).Item("StateAbbrev") = results2.Rows(0).Item("State") Then%>
                    <option value="<%=results.Rows(i).Item("StateAbbrev") %>" selected><%=results.Rows(i).Item("StateName") %></option>
                    <%else %>
                    <option value="<%=results.Rows(i).Item("StateAbbrev") %>"><%=results.Rows(i).Item("StateName") %></option>
                    <%End if%>
                    <%next %>
                </select>
            </div>
            <div class="required four wide field">
                <label>Zip</label>
                <input type="text" name="Zip" value="<%=results2.Rows(0).Item("Zip") %>" placeholder="Zip">
            </div>
        </div>
        <div class="fields">
            <div class="two wide field">
                <label>GeoLocate</label>
                <input onclick="updateLatLng(this)" type="button" value="Geolocate" class="fluid ui blue button">
            </div>
            <script>
                function updateLatLng(button) {
                    $(button).addClass('loading')
                    var form = $('[name=editBusiness]')[0]
                    var address = $(form).find('[name=Address]').val() + ' ' +
                        $(form).find('[name=Address2]').val() + ', ' +
                        $(form).find('[name=City]').val() + ', ' +
                        $(form).find('[name=State]').val() + $(form).find('[name=Zip]').val()
                    geoLocate(address, function (data) {
                        var form = $('[name=editBusiness]')[0]
                        $(form).find('[name=Lat]').val(data.lat)
                        $(form).find('[name=Long]').val(data.long)
                    }, button);
                }
            </script>
            <div class="required seven wide field">
                <labelfor="Lat">Latitude</labelfor="Lat">
                <input id="Lat" type="text" name="Lat" placeholder="Latitude" value="<%=results2.Rows(0).Item("Lat") %>">
            </div>
            <div class="required seven wide field">
                <label for="Long">Longitude</label>
                <input id="Long" type="text" name="Long" placeholder="Longitude" value="<%=results2.Rows(0).Item("Long") %>">
            </div>
        </div>

    </form>    
        
           
   
        
                  
    
    <script>
        $('[name=Phone]').mask('(000) 000-0000')
        $('select.dropdown').dropdown();
        $('form')
  .form({
      on: 'blur',
      fields: {
          EmployerName: {
              identifier: 'EmployerName',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Business Name.'
                }
              ]
          },
          Categories: {
              identifier: 'Categories',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please select a Category.'
                }
              ]
          },
          Phone: {
              identifier: 'Phone',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Phone Number.'
                }
              ]
          },        
          ShortDescription: {
              identifier: 'ShortDescription',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Description.'
                }
              ]
          },
          Address: {
              identifier: 'Address',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter an Address.'
                }
              ]
          },
          City: {
              identifier: 'City',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a City.'
                }
              ]
          },
          State: {
              identifier: 'State',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please select a State.'
                }
              ]
          },
          Zip: {
              identifier: 'Zip',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Zip.'
                }
              ]
          },
          Lat: {
              identifier: 'Lat',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Latitude.'
                }
              ]
          },
          Long: {
              identifier: 'Long',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Longitude.'
                }
              ]
          }
      }
  })
        ;
    </script>
</asp:Content>
