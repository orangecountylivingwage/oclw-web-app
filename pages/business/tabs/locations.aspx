﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Blank.Master" %>
<%@ MasterType VirtualPath="~/masters/Blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%  Dim params As New NameValueCollection
        Dim States As DataTable = Master.executeStoreProcedure("State_Log", params).Tables(0)
        params.Add("@Id", Request("Id"))
        Dim LocationLog As DataTable = Master.executeStoreProcedure("EmployerLocation_By_Employer_Id", params).Tables(0)

         %>
    <form name="newLocation" method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="EmployerLocation_Edit" />
        <input type="hidden" name="edit" value="1" />
        <input type="hidden" name="Id" value="<%=Request("Id")%>" />
        <input type="hidden" name="return" value="Id" />
        <input type="hidden" name="target" value="business/business_manage" />
        <h4 class="ui dividing header">Location Information</h4>
        <div class="fields">
            <div class="required eight wide field">
                <label for="LocationName">Location Name</label>
                <input type="text" name="LocationName" id="LocationName" placeholder="Location Name">
            </div>
            <div class="required eight wide field">
                <label>Phone</label>
                <input type="text" name="Phone" placeholder="Phone">
            </div>
        </div>
        <div class="field">
            <label for="ShortDescription">Override Description</label>
            <textarea maxlength="255" name="ShortDescription" id="ShortDescription"></textarea>
        </div>

        <h4 class="ui dividing header">Location Address</h4>
        <div class="fields">
            <div class="required eight wide field">
                <label for="Address">Address 1</label>
                <input type="text" name="Address" id="Address" placeholder="Address">
            </div>
            <div class="eight wide field">
                <label for="Address2">Address 2</label>
                <input type="text" name="Address2" id="Address2" placeholder="Address 2">
            </div>
        </div>
        <div class="fields">
            <div class="required six wide field">
                <label for="City">City</label>
                <input type="text" name="City" id="City" placeholder="City">
            </div>
            <div class="required six wide field">
                <label for="State">State</label>
                <select name="State" id="State" class="ui search dropdown">
                    <option value="">State</option>
                    <option value="">State</option>
                    <%For i As Integer = 0 To States.Rows().Count - 1%>
                    <option value="<%=States.Rows(i).Item("StateAbbrev") %>"><%=States.Rows(i).Item("StateName") %></option>
                    <%next %>
                </select>
            </div>
            <div class="required four wide field">
                <label for="Zip">Zip</label>
                <input type="text" name="Zip" id="Zip" placeholder="Zip">
            </div>
        </div>
        <div class="fields">
            <div class="two wide field">
                <label>GeoLocate</label>
                <input onclick="updateLatLng(this)" type="button" value="Geolocate" class="fluid ui blue button" name="" onclick="">
            </div>
            <script>
                function updateLatLng(button) {
                    $(button).addClass('loading')
                    var form = $('[name=newLocation]')[0]
                    var address = $(form).find('[name=Address]').val() + ' ' +
                        $(form).find('[name=Address2]').val() + ', ' +
                        $(form).find('[name=City]').val() + ', ' +
                        $(form).find('[name=State]').val() + $(form).find('[name=Zip]').val()
                    geoLocate(address, function (data) {
                        var form = $('[name=newLocation]')[0]
                        $(form).find('[name=Lat]').val(data.lat)
                        $(form).find('[name=Long]').val(data.long)
                    }, button);
                }
            </script>
            <div class="required seven wide field">
                <label>Latitude</label>
                <input type="text" name="Lat" placeholder="Latitude">
            </div>
            <div class="required seven wide field">
                <label>Longitude</label>
                <input type="text" name="Long" placeholder="Longitude">
            </div>
        </div>
        <div class="sixteen wide column" style="">
            <input type="submit" value="Add Location" class="fluid ui green basic button" name="" onclick="">
        </div>

        <div class="sixteen wide column" style="">
            <div class="ui error message"></div>
        </div>


    </form>
    <h4 class="ui dividing header">Location List</h4>
    <table name="mainTable" class="ui celled table">
        <thead>  
            <th>Location Name</th>       
            <th>Phone</th>     
            <th>Address</th>            
            <th>Override Description</th>
            <th style="display:none">Menu</th>
        </thead>
        <tbody>
            <%For i As Integer = 0 To LocationLog.Rows.Count - 1 %>
                <tr>  
                    <td>
                        <%=LocationLog.Rows(i).Item("LocationName") %>
                    </td>  
                    <td>
                        <%=LocationLog.Rows(i).Item("Phone") %>
                    </td>                   
                    <td>
                        <%=LocationLog.Rows(i).Item("full_address") %>
                    </td>                   
                    <td>
                        <%=LocationLog.Rows(i).Item("ShortDescription") %>
                    </td>
                    <td style="display:none">
                        <div name="mainTableMenu" class="ui vertical menu">
                          <div onclick="editPopup('/pages/business/popup/location_edit.aspx?Id=<%=LocationLog.Rows(i).Item("Id") %>')" class="link item active"> 
                            Edit
                          </div>                         
                          <div onclick="deletePopup('/pages/business/popup/location_delete.aspx?Id=<%=LocationLog.Rows(i).Item("Id") %>')"class="link item active"> 
                            Delete 
                         </div> 
                        </div>
                    </td>
                </tr>
            <%next %>
        </tbody>
    </table>    
    <script>
       
        $('[name=mainTable] tbody tr').bind("contextmenu","tr", function (event) {            
            event.preventDefault();

            $('[name=mainTableMenu].active').remove();            
            var menu = $($(event.currentTarget).find('td').last().html())
            $(menu).addClass('active')
            $('body').append(menu)
            
            $(menu).css({
                position: 'absolute',
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        });
        $('table[name=mainTable]').DataTable({


        });
        $(document).click(function (e) {
            if (!$(e.target).parents("[name=mainTableMenu].active").length > 0) {
                $('[name=mainTableMenu].active').remove();
            }
        })
        function editPopup(link) {
            var modal = $('<div class="ui modal" >'+
                               ' <div class="header">'+
                                 ' Edit Location'+
                               ' </div>')
            var content = $(' <div class="content"></div>')

            var buttons = $(' <div class="actions">'+
             ' <div class="ui negative button">'+
             '   Cancel'+
             ' </div>'+
              '<div class="ui positive approve right labeled icon button">'+
               ' Save'+
               ' <i class="checkmark icon"></i>'+
             ' </div>'+
           ' </div>'+
         ' </div>')
            $(modal).append(content)
            $(modal).append(buttons)
            $(content).load(link, function () {
                
                $(modal).modal({
                    onApprove: function () {
                        $(this).find('form').submit()
                    }
                }).modal('show')
            });
            
        }
        function deletePopup(link) {
            var modal = $('<div class="ui modal" >' +
                               ' <div class="header">' +
                                 ' Delete Location' +
                               ' </div>')
            var content = $(' <div class="content"></div>')

            var buttons = $(' <div class="actions">' +
             ' <div class="ui negative button">' +
             '   Cancel' +
             ' </div>' +
              '<div class="ui positive approve right labeled icon button">' +
               ' Yes' +
               ' <i class="checkmark icon"></i>' +
             ' </div>' +
           ' </div>' +
         ' </div>')
            $(modal).append(content)
            $(modal).append(buttons)
            $(content).load(link, function () {

                $(modal).modal({
                    onApprove: function () {
                        $(this).find('form').submit()
                        return false;
                    }
                }).modal('show')
            });

        }
    </script> 
        <script>
        $('select.dropdown')
  .dropdown()
            ;
        $('[name=Phone]').mask('(000) 000-0000')
        $('form').form({
      on: 'blur',
      fields: {
          LocationName: {
              identifier: 'LocationName',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Location Name.'
                }
              ]
          },
          Address: {
              identifier: 'Address',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter an Address.'
                }
              ]
          },
          City: {
              identifier: 'City',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a City.'
                }
              ]
          },
          State: {
              identifier: 'State',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please select a State.'
                }
              ]
          },
          Zip: {
              identifier: 'Zip',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Zip.'
                }
              ]
          },
          Lat: {
              identifier: 'Lat',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Latitude.'
                }
              ]
          },
          Long: {
              identifier: 'Long',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Longitude.'
                }
              ]
          }
      }
  });
    </script>   
</asp:Content>
