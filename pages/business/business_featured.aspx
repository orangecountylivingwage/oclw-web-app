﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%  Dim params As New NameValueCollection
    Dim results As DataTable = Master.executeStoreProcedure("Employer_Log", params).Tables(0)

%>
<form id="setFeatured" name="setFeatured" method="post" action="/aspx/application_update.aspx" class="ui form">
    <input type="hidden" name="procname" value="Employer_SetFeatured" />
    <input type="hidden" name="edit" value="1" /> 
    <input type="hidden" name="target" value="business/business_featured" />
    <input type="hidden" name="id" id="id" value="" /> 
    <input type="submit" style="display:none" /> 
</form>

    <table name="mainTable" class="ui celled table">
        <thead>
            <th>Business Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Description</th>
            <th>Featured</th>
        </thead>
        <tbody>
            <%For i As Integer = 0 To results.Rows.Count - 1 %>
                <tr <%=If(results.Rows(i).Item("Featured") = True, "class='positive'", "") %>>
                    <td>
                        <%=results.Rows(i).Item("EmployerName") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("full_address") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("Phone") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("Email") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("ShortDescription") %>
                    </td>
                    <td>
                        <button class="ui green basic button" onclick="submitForm(<%=results.Rows(i).Item("Id") %>);"><%=If(results.Rows(i).Item("Featured") = True, "Remove From Featured", "Add To Featured") %></button>
                    </td>
                </tr>

            <%next %>
        </tbody>
    </table>


<script>
    $('table').DataTable({});

    function submitForm(id) {
        $("#id").val(id)
        $("#setFeatured").submit();
    }
</script>    
</asp:Content>
