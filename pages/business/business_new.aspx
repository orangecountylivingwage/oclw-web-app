﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server"> 
    
    <%
        Dim params As New NameValueCollection
        Dim results As DataTable = Master.executeStoreProcedure("Category_By_Bottom", params).Tables(0) %>
    <form name="newBusiness" method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="Employer_Edit" />
        <input type="hidden" name="edit" value="1" /> 
        <input type="hidden" name="target" value="business/business_log" /> 

        <h4 class="ui dividing header">Business Information</h4>     
        <div class="fields">
            <div class="required ten wide field">
                <label for="EmployerName">Name</label>
                <input type="text" name="EmployerName" id="EmployerName" placeholder="Name">
            </div>    
             <div class="required six wide field">
                <label for="Categories">Category</label>
                <select id="Categories" name="Categories" class="ui search dropdown" multiple>
                    <%For i As Integer = 0 To results.Rows().Count - 1%>                           
                     <option value="<%=results.Rows(i).Item("id") %>"><%=results.Rows(i).Item("CategoryAndParent") %></option>
                    <%next %>
                </select>
            </div>       
        </div>  
        <div class="fields">
            <div class="required six wide field">
                <label for="Phone">Phone</label>
                <input id="Phone" type="text" name="Phone" placeholder="Phone">
            </div>
            <div class="five wide field">
                <label for="Website">Website</label>
                <input id="Website" type="text" name="Website" placeholder="Website">
            </div>
            <div class="five wide field">
                <label for="Email">Email</label>
                <input id="Email" type="text" name="Email" placeholder="Email">
            </div>
        </div>       
        <div class="required field">
            <label for="ShortDescription">Description</label>
            <textarea id="ShortDescription" maxlength="255" name="ShortDescription"></textarea>
          </div>
        <h4 class="ui dividing header">Business Address</h4>
        <div class="fields">
            <div class="required eight wide field">
                <label for="Address">Address 1</label>
                <input id="Address" type="text" name="Address" placeholder="Address">
            </div>
            <div class="eight wide field">
                <label for="Address2">Address 2</label>
                <input id="Address2" type="text" name="Address2" placeholder="Address 2">
            </div>
        </div>
        <div class="fields">
            <div class="required six wide field">
                <label for="City">City</label>
                <input id="City" type="text" name="City" placeholder="City">
            </div>
            <div class="required six wide field">
                <label for="State">State</label>
                <select id="State" name="State" class="ui search dropdown">
                        <option value="">State</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>
            </div>
            <div class="required four wide field">
                <label for="Zip">Zip</label>
                <input id="Zip" type="text" name="Zip" placeholder="Zip">
            </div>
        </div>
        <div class="fields">
            <div class="two wide field">
                <label>GeoLocate</label>
                <input onclick="updateLatLng(this)" type="button" value="Geolocate" class="fluid ui blue button" name="" onclick="">
            </div>
            <script>
                function updateLatLng(button) {
                    $(button).addClass('loading')
                    var form = $('[name=newBusiness]')[0]
                    var address = $(form).find('[name=Address]').val() + ' ' +
                        $(form).find('[name=Address2]').val() + ', ' +
                        $(form).find('[name=City]').val() + ', ' +
                        $(form).find('[name=State]').val() + $(form).find('[name=Zip]').val()
                    geoLocate(address, function (data) {
                        var form = $('[name=newBusiness]')[0]
                        $(form).find('[name=Lat]').val(data.lat)
                        $(form).find('[name=Long]').val(data.long)
                    },button);
                }
            </script>
            <div class="required seven wide field">
                <label for="Lat">Latitude</label>
                <input id="Lat" type="text" name="Lat" placeholder="Latitude">
            </div>
            <div class="required seven wide field">
                <label for="Long">Longitude</label>
                <input id="Long" type="text" name="Long" placeholder="Longitude">
            </div>
        </div>  
        <div class="sixteen wide column" style="">
	        <input type="submit" value="Create Business" class="fluid ui green button" name="" onclick="">
        </div>
         <div class="sixteen wide column" style="">
	        <div class="ui error message"></div>
        </div>
        
                  
    </form>
    <script>
        $('[name=Phone]').mask('(000) 000-0000')
        $('select.dropdown')
  .dropdown()
        ;
        $('form')
  .form({
      on: 'blur',
      fields: {
          EmployerName: {
              identifier: 'EmployerName',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Business Name.'
                }
              ]
          },
          Categories: {
              identifier: 'Categories',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please select a Category.'
                }
              ]
          },
          Phone: {
              identifier: 'Phone',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Phone Number.'
                }
              ]
          },         
          ShortDescription: {
              identifier: 'ShortDescription',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Description.'
                }
              ]
          },
          Address: {
              identifier: 'Address',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter an Address.'
                }
              ]
          },
          City: {
              identifier: 'City',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a City.'
                }
              ]
          },
          State: {
              identifier: 'State',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please select a State.'
                }
              ]
          },
          Zip: {
              identifier: 'Zip',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Zip.'
                }
              ]
          },
          Lat: {
              identifier: 'Lat',
            rules: [
              {
                  type: 'empty',
                  prompt: 'Please enter a Latitude.'
              }
            ]
          },
          Long: {
              identifier: 'Long',
              rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a Longitude.'
                }
              ]
          }
      }
  })
        ;
    </script>
</asp:Content>
