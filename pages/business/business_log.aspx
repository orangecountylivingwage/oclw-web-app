﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%  Dim params As New NameValueCollection
        Dim results As DataTable = Master.executeStoreProcedure("Employer_Log", params).Tables(0)

         %>
    <table name="mainTable" class="ui celled table">
        <thead>
            <th>Business Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Description</th>
            <th style="display:none">Menu</th>
        </thead>
        <tbody>
            <%For i As Integer = 0 To results.Rows.Count - 1 %>
                <tr>
                    <td>
                        <%=results.Rows(i).Item("EmployerName") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("full_address") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("Phone") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("Email") %>
                    </td>
                    <td>
                        <%=results.Rows(i).Item("ShortDescription") %>
                    </td>
                    <td style="display:none">
                        <div name="mainTableMenu" class="ui vertical menu">
                          <div onclick="editPopup('/pages/business/popup/business_edit.aspx?Id=<%=results.Rows(i).Item("Id") %>')" class="link item active"> 
                            Edit
                          </div>
                         <div class="link item active">
                            <a href="business_manage.aspx?Id=<%=results.Rows(i).Item("Id") %>">Manage</a>
                          </div>
                          <div onclick="deletePopup('/pages/business/popup/business_delete.aspx?Id=<%=results.Rows(i).Item("Id") %>')"class="link item active"> 
                            Delete 
                         </div> 
                        </div>
                    </td>
                </tr>

            <%next %>
        </tbody>
    </table>    
    <script>
       
        $('[name=mainTable] tbody tr').bind("contextmenu","tr", function (event) {            
            event.preventDefault();

            $('[name=mainTableMenu].active').remove();            
            var menu = $($(event.currentTarget).find('td').last().html())
            $(menu).addClass('active')
            $('body').append(menu)
            
            $(menu).css({
                position: 'absolute',
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        });
        $('table').DataTable({


        });
        $(document).click(function (e) {
            if (!$(e.target).parents("[name=mainTableMenu].active").length > 0) {
                $('[name=mainTableMenu].active').remove();
            }
        })
        function editPopup(link) {
            var modal = $('<div class="ui modal" >'+
                               ' <div class="header">'+
                                 ' Edit Business'+
                               ' </div>')
            var content = $(' <div class="content"></div>')

            var buttons = $(' <div class="actions">'+
             ' <div class="ui negative button">'+
             '   Cancel'+
             ' </div>'+
              '<div class="ui positive approve right labeled icon button">'+
               ' Save'+
               ' <i class="checkmark icon"></i>'+
             ' </div>'+
           ' </div>'+
         ' </div>')
            $(modal).append(content)
            $(modal).append(buttons)
            $(content).load(link, function () {
                
                $(modal).modal({
                    onApprove: function () {
                        $(this).find('form').submit()
                        return false;
                    }
                }).modal('show')
            });
            
        }
        function deletePopup(link) {
            var modal = $('<div class="ui modal" >' +
                               ' <div class="header">' +
                                 ' Delete Business' +
                               ' </div>')
            var content = $(' <div class="content"></div>')

            var buttons = $(' <div class="actions">' +
             ' <div class="ui negative button">' +
             '   Cancel' +
             ' </div>' +
              '<div class="ui positive approve right labeled icon button">' +
               ' Yes' +
               ' <i class="checkmark icon"></i>' +
             ' </div>' +
           ' </div>' +
         ' </div>')
            $(modal).append(content)
            $(modal).append(buttons)
            $(content).load(link, function () {

                $(modal).modal({
                    onApprove: function () {
                        $(this).find('form').submit()
                    }
                }).modal('show')
            });

        }
    </script>    
</asp:Content>
