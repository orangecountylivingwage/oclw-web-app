﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.data" %>
<script runat="server">
   function buildCategory(ByVal id as integer) as string
        dim builtResponse as string = ""
        dim params As New NameValueCollection
        params.Add("@ParentId", id)
        Dim subCategories As DataTable = Master.executeStoreProcedure("Category_By_ParentId", params).Tables(0)
        for i as integer = 0 to subCategories.rows().count - 1
            builtResponse = builtResponse & "<div class=""title"">" 
            builtResponse = builtResponse & subCategories.rows(0).item("CategoryName") & "</div>"
            builtResponse = builtResponse & "<div class=""active content"">" 
            builtResponse = builtResposne & buildCategory(subCategories.rows(0).item("id")) & "</div>"
        next
    end function
</script>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div name="categoryAccordian" class="ui styled accordion">
        <%=buildCategory(request("id") %>
    </div>
    <script>
        $('[name=categoryAccordian]').accordion({
            exclusive: 'false'
        })
    </script>
</asp:Content>
