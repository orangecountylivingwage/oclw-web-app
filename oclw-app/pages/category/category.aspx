﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%dim params As New NameValueCollection
        params.Add("@ParentId", -1)
        Dim topLevel As DataTable = Master.executeStoreProcedure("Category_By_ParentId", params).Tables(0) %>
    <div name="tabs" class="ui top attached tabular menu">
        <div onclick="loadCategory(<%=topLevel.rows(0).item("id") %>" class="active item"><%=topLevel.rows(0).item("CategoryName") %></div>
        <%For i as integer = 1 to results.rows().count - 1 %>
        <div onclick="loadCategory(<%=topLevel.rows(i).item("id") %>" class="item"><%=topLevel.rows(i).item("CategoryName") %></div>
        <%next %>
    </div>
    <div name="tabcontainer" class="ui bottom attached active tab segment">
        
    </div>
    <script>
        function loadTab(id) {
            $('[name=tabcontainer]').addClass('loading')
            $('[name=tabcontainer]').load('/pages/category/categoryOrganizer.aspx?id=' + id, function () {
                $('[name=tabcontainer]').removeClass('loading')
            })
        }

        loadTab(<%=topLevel.rows(0).item("id") %>);
    </script>
</asp:Content>
