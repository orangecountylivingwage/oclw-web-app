﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
            <%  
    Dim params As New NameValueCollection
    Dim results As DataTable = Master.executeStoreProcedure("User_List", params).Tables(0)

    If Session("LoggedIn") Is Nothing OrElse Session("LoggedIn") = False Then
        Session("ReturnPage") = "pages/business/business_new.aspx"
        Response.Redirect("/pages/user/login.aspx")
    End If

%>
    <%
        Dim params As New NameValueCollection
        Dim results As DataTable = Master.executeStoreProcedure("Category_By_Bottom", params).Tables(0) %>
    <form method="post" action="/aspx/application_update.aspx" class="ui form">
        <input type="hidden" name="procname" value="Employer_Edit" />
        <input type="hidden" name="edit" value="1" /> 
        <input type="hidden" name="target" value="business/business_log" /> 
        <h4 class="ui dividing header">Business Information</h4>     
        <div class="fields">
            <div class="ten wide field">
                <label>Name</label>
                <input type="text" name="EmployerName" placeholder="Name">
            </div>    
             <div class="six wide field">
                <label>Category</label>
                <select name="CategoryId" class="ui search dropdown">
                    <%For i As Integer = 0 To results.Rows().Count - 1%>                           
                     <option value="<%=results.Rows(i).Item("id") %>"><%=results.Rows(i).Item("CategoryName") %></option>
                    <%next %>
                </select>
            </div>       
        </div>  
        <div class="fields">
            <div class="six wide field">
                <label>Phone</label>
                <input type="text" name="Phone" placeholder="Email">
            </div>
            <div class="five wide field">
                <label>Website</label>
                <input type="text" name="Website" placeholder="Website">
            </div>
            <div class="five wide field">
                <label>Email</label>
                <input type="text" name="Email" placeholder="Email">
            </div>
        </div>       
        <div class="field">
            <label>Description</label>
            <textarea name="ShortDescription"></textarea>
          </div>
        <h4 class="ui dividing header">Business Address</h4>
        <div class="fields">
            <div class="eight wide field">
                <label>Address 1</label>
                <input type="text" name="Address" placeholder="Address">
            </div>
            <div class="eight wide field">
                <label>Address 2</label>
                <input type="text" name="Address2" placeholder="Address 2">
            </div>
        </div>
        <div class="fields">
            <div class="six wide field">
                <label>City</label>
                <input type="text" name="City" placeholder="City">
            </div>
            <div class="six wide field">
                <label>State</label>
                <select name="State" class="ui search dropdown">
                        <option value="">State</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>
            </div>
            <div class="four wide field">
                <label>Zip</label>
                <input type="text" name="Zip" placeholder="Zip">
            </div>
        </div>        
        <div class="sixteen wide column" style="">
	        <input type="submit" value="Create Business" class="fluid ui green basic button" name="" onclick="">
        </div>
        
                  
    </form>
    <script>
        $('select.dropdown')
  .dropdown()
        ;
    </script>
</asp:Content>
