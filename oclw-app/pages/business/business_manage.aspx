﻿<%@ Page Title="Business Manage" Language="VB" MasterPageFile="~/masters/Site.Master" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <%  
    Dim params As New NameValueCollection
    Dim results As DataTable = Master.executeStoreProcedure("User_List", params).Tables(0)

    If Session("LoggedIn") Is Nothing OrElse Session("LoggedIn") = False Then
        Session("ReturnPage") = "pages/business/business_manage.aspx?Id=" & Request("id")
        Response.Redirect("/pages/user/login.aspx")
    End If

%>
    <%Dim params As New NameValueCollection
        params.Add("@Id", Request("id"))
        Dim employerInfo As DataTable = Master.executeStoreProcedure("Employer_By_Id", params).Tables(0)
        Dim employerSearchInfo As DataTable = Master.executeStoreProcedure("Employer_History_Count_By_ID", params).Tables(0)
        %>
    <div class="ui grid">
        <div class="two column row">
            <div class="column">
                <div id="EmployerInfo">
                    <table class="ui striped table">
                        <thead>
                            <tr>
                                <th colspan="2"><%=employerInfo.Rows(0).Item("EmployerName")%> Information</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Phone</td>
                                <td><%=employerInfo.Rows(0).Item("Phone")%></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><%=employerInfo.Rows(0).Item("Email")%></td>
                            </tr>
                            <tr>
                                <td>Webste</td>
                                <td><%=employerInfo.Rows(0).Item("Website")%></td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td><%=employerInfo.Rows(0).Item("ShortDescription")%></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="column">
                <div id="ClickHistory">
                    <table class="ui striped table">
                        <thead>
                            <tr>
                                <th colspan="2">Click History</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%For i As Integer = 0 To employerSearchInfo.Rows().Count - 1%>
                            <tr>
                                <td><%=employerSearchInfo.Rows(i).Item("title")%></td>
                                <td><%=employerSearchInfo.Rows(i).Item("NumViews")%></td>
                            </tr>
                            <%  Next %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui top attached tabular menu">
        <div onclick="loadCategory(this,'locations.aspx?Id=<%=Request("id") %>')" class="active item">Locations</div>       
    </div>
    <div name="tabContent" class="ui bottom attached active tab segment">
        
    </div>
    <script>
        function loadCategory(element,id){
            $('.active.item').removeClass("active")
            $(element).addClass("active");
            $('[name=tabContent]').addClass("loading").load('/pages/business/tabs/' + id, function(){
                $('[name=tabContent]').removeClass("loading")
            })
        }
        loadCategory(this,'locations.aspx?Id=<%=Request("id") %>')
    </script>
</asp:Content>
