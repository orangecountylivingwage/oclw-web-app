﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<%@ MasterType VirtualPath="~/masters/Site.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<%
    If Session("LoggedIn") Is Nothing OrElse Session("LoggedIn") = False Then
        Session("ReturnPage") = "default.aspx"
        Response.Redirect("/pages/user/login.aspx")
    End If

    Dim topEmpParams As New NameValueCollection
    topEmpParams.Add("@StartDate", Today.AddDays(-7))
    topEmpParams.Add("@EndDate", Today)
    topEmpParams.Add("@top", 10)
    Dim topEmpResults As DataTable = Master.executeStoreProcedure("Employer_History_Count", topEmpParams).Tables(0)

    Dim topSearchParams As New NameValueCollection
    topSearchParams.Add("@StartDate", Today.AddDays(-7))
    topSearchParams.Add("@EndDate", Today)
    topSearchParams.Add("@top", 10)
    Dim topSearchResults As DataTable = Master.executeStoreProcedure("Search_History_Count", topSearchParams).Tables(0)
%>

    <div class="ui grid">
        <div class="two column row">
            <div class="column">
                <h1>Top 10 Employers Past 7 Days</h1>
                <div id="TopEmployers">
                    <table class="ui striped table">
                        <thead>
                            <tr>
                                <th>Employer</th>
                                <th>Total Views</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% For i As Integer = 0 To topEmpResults.Rows.Count - 1 %>
                                <tr>
                                    <td><% =topEmpResults.Rows(i).Item("EmployerName") %></td>
                                    <td><% =topEmpResults.Rows(i).Item("NumViews") %></td>
                                </tr>
                            <% Next %>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="column">
                <h1>Top 10 Search Queries Past 7 Days</h1>
                <div id="TopSearches">
                    <table class="ui striped table">
                        <thead>
                            <tr>
                                <th>Search Query</th>
                                <th># Searches</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% For i As Integer = 0 To topSearchResults.Rows.Count - 1 %>
                                <tr>
                                    <td><% =topSearchResults.Rows(i).Item("SearchTerms") %></td>
                                    <td><% =topSearchResults.Rows(i).Item("NumSearches") %></td>
                                </tr>
                            <% Next %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="two column row">
            <div class="column"></div>
            <div class="column"></div>
        </div>
    </div>
</asp:Content>
