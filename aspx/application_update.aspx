﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" debug="true" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%Dim params As New NameValueCollection
    params.Add("@ProcedureName", Request("procname"))
    Dim parameters As DataTable = Master.executeStoreProcedure("Procedure_Parameters", params).Tables(0)


    params.Clear()

    For i As Integer = 0 To parameters.Rows.Count() - 1
        REM Response.Write(parameters.Rows(i).Item("Name") & " " & Request(Replace(parameters.Rows(i).Item("Name"), "@", "")) & "<br>")

        If Not Request(Replace(parameters.Rows(i).Item("Name"), "@", "")) Is Nothing AndAlso Request(Replace(parameters.Rows(i).Item("Name"), "@", "")) <> "" Then
            params.Add(parameters.Rows(i).Item("Name"), Request(Replace(parameters.Rows(i).Item("Name"), "@", "")))
        End If
    Next
    Dim results As DataSet = Master.executeStoreProcedure(Request("procname"), params)
    If Request("target") <> "" Then
        If Request("return") <> "" Then
            If Request("return2") <> "" Then
                If Request("return3") <> "" Then
                    Response.Redirect("/pages/" & Request("target") & ".aspx?" & Request("return") & "=" & results.Tables(0).Rows(0).Item(Request("return")) & "&" & Request("return2") & "=" & results.Tables(0).Rows(0).Item(Request("return2")) & "&" & Request("return3") & "=" & results.Tables(0).Rows(0).Item(Request("return3")))
                Else
                    Response.Redirect("/pages/" & Request("target") & ".aspx?" & Request("return") & "=" & results.Tables(0).Rows(0).Item(Request("return")) & "&" & Request("return2") & "=" & results.Tables(0).Rows(0).Item(Request("return2")))
                End If
            Else
                Response.Redirect("/pages/" & Request("target") & ".aspx?" & Request("return") & "=" & results.Tables(0).Rows(0).Item(Request("return")))
            End If
        Else
            Response.Redirect("/pages/" & Request("target") & ".aspx")
        End If

    End If
    If Request("return") <> "" Then
        If Request("return2") <> "" Then
            If Request("return3") <> "" Then
                Response.Write("{""" & Request("return") & """:""" & results.Tables(0).Rows(0).Item(Request("return")) & """,""" & Request("return2") & """:""" & results.Tables(0).Rows(0).Item(Request("return2")) & """,""" & Request("return3") & """:""" & results.Tables(0).Rows(0).Item(Request("return3")) & """}")
            Else
                Response.Write("{""" & Request("return") & """:""" & results.Tables(0).Rows(0).Item(Request("return")) & """,""" & Request("return2") & """:""" & results.Tables(0).Rows(0).Item(Request("return2")) & """}")
            End If
        Else
            Response.Write("{""" & Request("return") & """:""" & results.Tables(0).Rows(0).Item(Request("return")) & """}")
        End If
    End If

     %>

</asp:Content>
