﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.data" %>
<%@ import namespace="system.security.cryptography" %>
<%@ import namespace="system.text" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    Dim params As New NameValueCollection

    Dim action As String = Request("action")

    If action = "update" Or action = "changePassword" Or action = "delete" Then
        params.Add("@Id", Request("Id"))
    End If

    If action = "insert" Or action = "update" Then
        params.Add("@Email", Request("Email"))
        params.Add("@FirstName", Request("FirstName"))
        params.Add("@LastName", Request("LastName"))
    End If

    If action = "insert" Or action = "changePassword" Then
        Dim saltBytes As Byte() = New Byte(7) {}
        Dim rng As RNGCryptoServiceProvider = New RNGCryptoServiceProvider()
        rng.GetNonZeroBytes(saltBytes)

        Dim passwordBytes As Byte() = Encoding.UTF8.GetBytes(Request("Password"))

        Dim passwordWithSaltBytes() As Byte = New Byte(passwordBytes.Length + saltBytes.Length - 1) {}

        Dim i As Integer
        For i = 0 To passwordBytes.Length - 1
            passwordWithSaltBytes(i) = passwordBytes(i)
        Next i

        For i = 0 To saltBytes.Length - 1
            passwordWithSaltBytes(passwordBytes.Length + i) = saltBytes(i)
        Next i

        Dim passwordHash As HashAlgorithm = New SHA256Managed()
        Dim passwordHashBytes As Byte() = passwordHash.ComputeHash(passwordWithSaltBytes)
        Dim passwordHashWithSaltBytes() As Byte = New Byte(passwordHashBytes.Length + saltBytes.Length - 1) {}

        For i = 0 To passwordHashBytes.Length - 1
            passwordHashWithSaltBytes(i) = passwordHashBytes(i)
        Next i

        For i = 0 To saltBytes.Length - 1
            passwordHashWithSaltBytes(passwordHashBytes.Length + i) = saltBytes(i)
        Next i

        Dim passwordHashValue As String = Convert.ToBase64String(passwordHashWithSaltBytes)

        params.Add("@Password", passwordHashValue)
        params.Add("@Salt", Convert.ToBase64String(saltBytes))
    End If

    If action = "insert" Then
        Master.executeStoreProcedure("User_Insert", params)
    ElseIf action = "update" Then
        Master.executeStoreProcedure("User_Update", params)
    ElseIf action = "changePassword" Then
        Master.executeStoreProcedure("User_ChangePassword", params)
    ElseIf action = "delete" Then
        Master.executeStoreProcedure("User_Delete", params)
    End If

    Response.Redirect("/pages/user/user_list.aspx")
%>

</asp:Content>
