﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.io" %>
<%@ import namespace="system.net" %>
<%@ import namespace="newtonsoft.json" %>
<%@ import namespace="newtonsoft.json.linq" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    Dim params As New NameValueCollection
    Dim searchRequest As HttpWebRequest
    Dim searchResponse As HttpWebResponse
    Dim searchReader As StreamReader

    Dim strQuery As String
    Dim strNoResults As String = "{""results"": []}"
    Dim intCount As Integer

    Dim jResults As JObject
    Dim jArray As JArray
    Dim strResults As String

    Dim dblLat As Double
    Dim dblLong As Double
    Dim dblLatDest As Double
    Dim dblLongDest As Double
    Dim deltaLat As Double
    Dim deltaLong As Double
    Dim a, c As Double
    Dim distance As Double

    Response.ContentType = "application/json"

    If Request("query") Is Nothing Then
        Response.Write(strNoResults)
    Else
        ' Insert search terms into hisory
        params.Add("@SearchTerms", Left(Request("query"), 100))
        Master.executeStoreProcedure("Search_History_Insert", params)

        ' Perform search
        strQuery = Trim(Request("query"))
        strQuery = strQuery.Replace(" ", "* ") & "*"
        searchRequest = DirectCast(WebRequest.Create("https://oclw.search.windows.net/indexes/idx-employers/docs?api-version=2015-02-28&api-key=32CEC26AE719201262AB7A6607BFA390&queryType=full&search=" & strQuery), HttpWebRequest)
        searchResponse = DirectCast(searchRequest.GetResponse(), HttpWebResponse)
        searchReader = New StreamReader(searchResponse.GetResponseStream())
        jResults = JObject.Parse(searchReader.ReadToEnd())

        If Not Request("lat") Is Nothing Then
            dblLat = Request("lat")
            dblLong = Request("long")

            jArray = DirectCast(jResults.SelectToken("value"), JArray)

            For Each row In jArray.Children(Of JObject)
                dblLatDest = Double.Parse(row.Property("Lat").Value)
                dblLongDest = Double.Parse(row.Property("Long").Value)

                deltaLat = (dblLatDest - dblLat) * (Math.PI / 180.0)
                deltaLong = (dblLongDest - dblLong) * (Math.PI / 180.0)

                ' Haversine Formula
                a = Math.Pow(Math.Sin(deltaLat / 2), 2) + (Math.Pow(Math.Sin(deltaLong / 2), 2) * Math.Cos(dblLat * (Math.PI / 180.0)) * Math.Cos(dblLatDest * (Math.PI / 180.0)))
                c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a))
                distance = c * 6371

                ' Convert KM to Mi
                distance = distance * 0.62137119

                ' Round
                distance = Math.Round(distance, 1)

                row.Property("Long").AddAfterSelf(New JProperty("Distance", distance))
            Next
        End If

        strResults = jResults("value").ToString()
        intCount = jResults("value").Count
        strResults = "{""result"": " & strResults & ", ""count"": " & intCount & "}"
        Response.Write(strResults)
    End If
%>

</asp:Content>
