﻿<%-- Developed By: Chris Lathrop --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    Dim params As New NameValueCollection
    Response.ContentType = "application/json"

    Dim reportType As String = Request("ReportType")
    Dim errResponse As String = ""
    Dim results As DataTable

    Select Case reportType
        Case "EmployerViewCount"
            If Not Request("StartDate") Is Nothing And Not Request("EndDate") Is Nothing Then
                params.Add("@StartDate", Request("StartDate"))
                params.Add("@EndDate", Request("EndDate"))

                If Not Request("top") Is Nothing Then
                    params.Add("@top", Request("top"))
                End If
                results = Master.executeStoreProcedure("Employer_History_Count", params).Tables(0)
            Else
                errResponse = "{""responseType"": ""Error"", ""message"": ""The API Call For the EmployerViewCount report expects parameters For Start And End dates.""}"
            End If
        Case "SearchTermCount"
            If Not Request("StartDate") Is Nothing And Not Request("EndDate") Is Nothing Then
                params.Add("@StartDate", Request("StartDate"))
                params.Add("@EndDate", Request("EndDate"))

                If Not Request("top") Is Nothing Then
                    params.Add("@top", Request("top"))
                End If
                results = Master.executeStoreProcedure("Search_History_Count", params).Tables(0)
            Else
                errResponse = "{""responseType"": ""Error"", ""message"": ""The API Call For the SearchTermCount report expects parameters For Start And End dates.""}"
            End If
        Case Else
            errResponse = "{""responseType"": ""Error"", ""message"": ""The ReportType parameter value specified does not match a valid report.""}"
    End Select

    If errResponse = "" Then
        Response.Write(Master.GetJson(results))
    Else
        Response.Write(errResponse)
    End If
%>

</asp:Content>
