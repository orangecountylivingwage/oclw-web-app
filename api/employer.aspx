﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%Dim params As New NameValueCollection
    Response.ContentType = "application/json"
    If Request("Id") Is Nothing Then
        params.Add("@Id", -1)
    Else
        params.Add("@Id", Request("Id"))
    End If

    Dim results As DataTable = Master.executeStoreProcedure("EmployerLocation_By_Id", params).Tables(0)
    Response.Write(Master.GetJson(results))
     %>

</asp:Content>
