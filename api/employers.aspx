﻿<%-- Developed By: Dan Piccot --%>
<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/masters/blank.Master" %>
<%@ MasterType VirtualPath="~/masters/blank.master" %>
<%@ import namespace="system.data" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%Dim params As New NameValueCollection
    Response.ContentType = "application/json"
    If Request("CategoryId") Is Nothing Then
        params.Add("@CategoryId", -1)
    Else
        params.Add("@CategoryId", Request("CategoryId"))
    End If

    If Not Request("lat") Is Nothing Then
        params.Add("@lat", Request("lat"))
        params.Add("@long", Request("long"))
    End If

    Dim results As DataTable = Master.executeStoreProcedure("EmployerLocation_By_CategoryId", params).Tables(0)
    Response.Write(Master.GetJson(results))
     %>

</asp:Content>
