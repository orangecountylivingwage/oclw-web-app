﻿function geoLocate(address, callback,button) {
    $.ajax({
        url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURIComponent(address)
    }
    ).done(function (data) {
        if (button) {
            $(button).removeClass('loading')
        }
        var item = {
            lat: data.results[0].geometry.location.lat,
            long: data.results[0].geometry.location.lng
        };        
        callback(item);
    });
}